package com.task.sarytask.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.task.sarytask.R
import com.task.sarytask.data.models.CatalogResult

/**
Created By M.EL-Wakeel
 */
class CatalogsAdapter(
    private val catalogs: ArrayList<CatalogResult>,
    private val context: Context,
) : RecyclerView.Adapter<CatalogsAdapter.CatalogViewHolder>() {

    class CatalogViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.catalog_item, parent, false)) {
        var catalogTitle = itemView.findViewById<TextView>(R.id.catalog_title)!!
        var dataList = itemView.findViewById<RecyclerView>(R.id.data_list)!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatalogViewHolder {
        val inflater = LayoutInflater.from(context)
        return CatalogViewHolder(
            inflater,
            parent
        )
    }

    override fun onBindViewHolder(holder: CatalogViewHolder, position: Int) {
        val catalog = catalogs[position]
        holder.catalogTitle.text = catalog.title
        holder.dataList.apply {
            layoutManager = object : GridLayoutManager(context, 4) {}
            adapter = DataAdapter(catalog.data!!, context, catalog.dataType!!)
            adapter!!.notifyDataSetChanged()
        }
    }

    override fun getItemCount() = catalogs.size

}