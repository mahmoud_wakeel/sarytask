package com.task.sarytask.ui.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.task.sarytask.R
import com.task.sarytask.data.models.Data
import com.task.sarytask.utils.Constants
import com.task.sarytask.utils.Utils
import java.util.*


/**
Created By M.EL-Wakeel
 */
class DataAdapter(
    private val dataList: ArrayList<Data>,
    private val context: Context,
    private val dataType: String,
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    class SmartViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dataImg: ImageView = itemView.findViewById(R.id.data_img)
        var dataName: TextView = itemView.findViewById(R.id.data_name)
    }

    class GroupViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dataImg: ImageView = itemView.findViewById(R.id.data_img)
    }

    class BannerViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dataImg: ImageView = itemView.findViewById(R.id.item_img)
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            dataType.toLowerCase(Locale.ROOT) == Constants.SMART.toLowerCase(
                Locale.ROOT) -> {
                0
            }
            dataType.toLowerCase(Locale.ROOT) == Constants.GROUP.toLowerCase(
                Locale.ROOT) -> {
                1
            }
            else -> {
                2
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            0 -> SmartViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.data_item, parent, false)
            )
            1 -> GroupViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.data_item, parent, false)
            )
            else -> BannerViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.banner_item, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = dataList[position]
        when (holder.itemViewType) {
            0 -> {
                val smartViewHolder = holder as SmartViewHolder
                Utils.loadImage(context, smartViewHolder.dataImg, item.image!!)
                smartViewHolder.dataName.text = item.name
            }
            1 -> {
                val groupViewHolder = holder as GroupViewHolder
                val layoutParams = ConstraintLayout.LayoutParams(210, 210)
                groupViewHolder.dataImg.layoutParams = layoutParams
                Utils.loadImage(context, groupViewHolder.dataImg, item.image!!)
            }
            2 -> {
                val bannerViewHolder = holder as BannerViewHolder
                Utils.loadImage(context, bannerViewHolder.dataImg, item.image!!)
            }
        }
    }

    override fun getItemCount() = dataList.size
}