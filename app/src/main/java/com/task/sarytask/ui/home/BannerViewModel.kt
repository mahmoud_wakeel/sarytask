package com.task.sarytask.ui.home

import androidx.lifecycle.MutableLiveData
import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.remote.RemoteDataSourceImpl
import com.task.sarytask.domain.SchedulerProviders
import com.task.sarytask.domain.idlingresource.EspressoIdlingResource
import com.task.sarytask.ui.base.BaseViewModel
import com.task.sarytask.utils.ViewState

/**
Created By M.EL-Wakeel
 */
class BannerViewModel(
    private val repositoryImpl: RemoteDataSourceImpl,
    private var scheduler: SchedulerProviders
) : BaseViewModel() {
    val bannerState = MutableLiveData<ViewState<BannersResponse>>()

    fun getBanners() {
        EspressoIdlingResource.increment()
        repositoryImpl.getBannerDataFromApi()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnNext {
                onLoading()
            }
            .subscribe({
                onSuccess(it)
            }, {
                onError(it)
            }
            ).also { compositeDisposable.add(it) }
    }

    private fun onSuccess(banners: BannersResponse) {
        bannerState.postValue(ViewState.success(banners))
        EspressoIdlingResource.decrement()
    }

    private fun onError(err: Throwable) {
        bannerState.postValue(ViewState.error(err))
        EspressoIdlingResource.decrement()
    }

    private fun onLoading() {
        bannerState.postValue(ViewState.loading())
    }
}