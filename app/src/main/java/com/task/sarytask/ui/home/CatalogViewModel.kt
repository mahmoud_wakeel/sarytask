package com.task.sarytask.ui.home

import androidx.lifecycle.MutableLiveData
import com.task.sarytask.data.models.CatalogResponse
import com.task.sarytask.data.remote.RemoteDataSourceImpl
import com.task.sarytask.domain.SchedulerProviders
import com.task.sarytask.domain.idlingresource.EspressoIdlingResource
import com.task.sarytask.ui.base.BaseViewModel
import com.task.sarytask.utils.ViewState

/**
Created By M.EL-Wakeel
 */
class CatalogViewModel(
    private val repositoryImpl: RemoteDataSourceImpl,
    private var scheduler: SchedulerProviders
) : BaseViewModel() {
    val catalogState = MutableLiveData<ViewState<CatalogResponse>>()

    fun getCatalogs() {
        EspressoIdlingResource.increment()
        repositoryImpl.getCatalogDataFromApi()
            .subscribeOn(scheduler.io())
            .observeOn(scheduler.ui())
            .doOnNext {
                onLoading()
            }
            .subscribe({
                onSuccess(it)
            }, {
                onError(it)
            }
            ).also { compositeDisposable.add(it) }
    }

    private fun onSuccess(catalogs: CatalogResponse) {
        catalogState.postValue(ViewState.success(catalogs))
        EspressoIdlingResource.decrement()
    }

    private fun onError(err: Throwable) {
        catalogState.postValue(ViewState.error(err))
        EspressoIdlingResource.decrement()
    }

    private fun onLoading() {
        catalogState.postValue(ViewState.loading())
    }
}