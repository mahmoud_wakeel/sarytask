package com.task.sarytask.ui.home

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.task.sarytask.R
import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.models.CatalogResponse
import com.task.sarytask.ui.home.adapter.CatalogsAdapter
import com.task.sarytask.ui.home.banner.BannersPagerAdapter
import com.task.sarytask.utils.ViewState
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.android.viewmodel.ext.android.viewModel

/**
Created By M.EL-Wakeel
 */
class MainActivity : AppCompatActivity() {
    private val bannerViewModel by viewModel<BannerViewModel>()
    private val catalogViewModel by viewModel<CatalogViewModel>()
    private lateinit var bannersPagerAdapter: BannersPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        observeBannersChanges()
        bannerViewModel.getBanners()
    }

    private fun observeBannersChanges() {
        bannerViewModel.bannerState.observe(this@MainActivity, {
            when (it.currentState) {
                ViewState.State.LOADING -> {

                }
                ViewState.State.SUCCESS -> {
                    observeCatalogsChanges()
                    catalogViewModel.getCatalogs()
                    observeBanners(it.data)
                }
                ViewState.State.FAILED -> {
                    it.err?.let { err -> observeError(err) }
                }
            }
        })
    }

    private fun observeCatalogsChanges() {
        catalogViewModel.catalogState.observe(this@MainActivity, {
            when (it.currentState) {
                ViewState.State.LOADING -> {

                }
                ViewState.State.SUCCESS -> {
                    observeCatalogs(it.data)
                }
                ViewState.State.FAILED -> {
                    it.err?.let { err -> observeError(err) }
                }
            }
        })
    }

    private fun observeError(err: Throwable) {
        err.message?.let { Log.e("TAG", it) }
    }

    private fun observeBanners(banners: BannersResponse?) {
        viewpager_banners.visibility = View.VISIBLE
        if (banners!!.result!!.size > 1)
            indicator.visibility = View.VISIBLE
        else
            indicator.visibility = View.GONE
        bannersPagerAdapter =
            BannersPagerAdapter(
                supportFragmentManager,
                this@MainActivity,
                banners.result!!
            )
        viewpager_banners.adapter = bannersPagerAdapter
        indicator.setViewPager(viewpager_banners)
    }

    private fun observeCatalogs(catalogs: CatalogResponse?) {
        catalog_list.apply {
            layoutManager = object : LinearLayoutManager(this@MainActivity){}
            adapter = CatalogsAdapter(catalogs!!.result!!,this@MainActivity)
            adapter!!.notifyDataSetChanged()
        }

        /*val dataList: ArrayList<MappedData> = ArrayList()
        for (i in 0 until catalogs!!.result!!.size) {
            for (j in 0 until catalogs.result!![i].data!!.size) {
                val mappedData = MappedData(
                    catalogs.result[i].data!![j].name,
                    catalogs.result[i].data!![j].image,
                    catalogs.result[i].dataType,
                    catalogs.result[i].showTitle,
                    catalogs.result[i].title
                )
                dataList.add(mappedData)
            }
        }
        catalog_list.apply {
            layoutManager = object : GridLayoutManager(context, 4) {}
            adapter = CatalogAdapter(dataList,
                this@MainActivity)
            adapter!!.notifyDataSetChanged()
        }*/
    }
}