package com.task.sarytask.ui.home.banner;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.task.sarytask.data.models.Result;

import java.util.ArrayList;
import java.util.List;

/**
 * Created By M.EL-Wakeel
 */
public class BannersPagerAdapter extends FragmentStatePagerAdapter {
    private Context context;
    private LayoutInflater inflater;
    private FragmentManager fragmentManager;
    private List<Result> banners;
    Drawable drawable;
    int pos;

    public BannersPagerAdapter(FragmentManager fragmentManager,
                               Context context, List<Result> banners) {
        super(fragmentManager);
        this.context = context;
        this.banners = banners;
        this.fragmentManager = fragmentManager;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public BannersPagerAdapter(FragmentManager fragmentManager,
                               Context context, Drawable drawable) {
        super(fragmentManager);
        this.context = context;
        this.drawable = drawable;
        this.fragmentManager = fragmentManager;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        if (banners == null)
            return 1;
        return banners.size();
    }

    public void setOffers(ArrayList<Result> banners) {
        this.banners = banners;
        notifyDataSetChanged();
    }

    @Override
    public Fragment getItem(int position) {
        if (drawable == null) {
            Result banner = banners.get(position);
            return BannerFragment.Companion.newInstance(banner);
        } else {
            BannerFragment activeBannersFragment = new BannerFragment();
            return activeBannersFragment.drawableInstance();
        }
    }
}