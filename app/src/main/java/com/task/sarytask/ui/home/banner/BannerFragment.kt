package com.task.sarytask.ui.home.banner

import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.task.sarytask.R
import com.task.sarytask.data.models.Result
import com.task.sarytask.utils.Constants
import com.task.sarytask.utils.Utils

/**
Created By M.EL-Wakeel
 */
class BannerFragment : Fragment() {
    private var banner: Result? = null

    companion object{
        fun newInstance(
            banner: Result
        ): BannerFragment {
            val fragmentFirst = BannerFragment()
            val args = Bundle()
            args.putParcelable(Constants.BANNER, banner)
            fragmentFirst.arguments = args
            return fragmentFirst
        }
    }

    fun drawableInstance(): BannerFragment {
        return BannerFragment()
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (arguments != null) {
            banner = requireArguments().getParcelable(Constants.BANNER)
        }
        val view: View = inflater.inflate(R.layout.banner_layout, container, false)
        val bannerContainer = view.findViewById<ImageView>(R.id.home_banner)
        Utils.loadImage(
            activity,
            bannerContainer,
            banner!!.image!!
        )

        bannerContainer.setOnClickListener {
            Toast.makeText(context, banner!!.link, Toast.LENGTH_SHORT).show()
        }
        return view
    }
}
