package com.task.sarytask

import android.app.Application
import com.task.sarytask.di.appModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
Created By M.EL-Wakeel
 */
class SaryApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@SaryApplication)
            modules(appModule)
        }
    }

}