package com.task.sarytask.di

import com.task.sarytask.data.remote.ApiServices
import com.task.sarytask.data.remote.RemoteDataSourceImpl
import com.task.sarytask.data.repo.ApiRepository
import com.task.sarytask.domain.SchedulerProviders
import com.task.sarytask.ui.home.BannerViewModel
import com.task.sarytask.ui.home.CatalogViewModel
import com.task.sarytask.utils.ApiInterceptor
import com.task.sarytask.utils.Constants
import com.task.sarytask.utils.ViewState
import okhttp3.OkHttpClient
import okhttp3.Protocol
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.CallAdapter
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
Created By M.EL-Wakeel
 */
val appModule = module {
    viewModel { BannerViewModel(get(), get()) }
    viewModel { CatalogViewModel(get(), get()) }

    single { SchedulerProviders.DEFAULT }
    single { ApiRepository(get()) }
    single { RemoteDataSourceImpl(get(), get()) }

    single { ViewState }
    single {
        HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }
    }
    single { ApiInterceptor() }
    single {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(get<HttpLoggingInterceptor>())
        builder.addInterceptor(get<ApiInterceptor>())
        builder.protocols(listOf(Protocol.HTTP_1_1, Protocol.HTTP_2))
        builder.build()
    }

    single<Converter.Factory> { GsonConverterFactory.create() }
    single<CallAdapter.Factory> { RxJava2CallAdapterFactory.create() }
    single<Retrofit> {
        Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addCallAdapterFactory(get())
            .addConverterFactory(get())
            .client(get())
            .build()
    }

    single {
        createApiService<ApiServices>(get())

    }
}

inline fun <reified T> createApiService(retrofit: Retrofit): T = retrofit.create(T::class.java)