package com.task.sarytask.domain

import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.models.CatalogResponse
import io.reactivex.Observable

/**
Created By M.EL-Wakeel
 */
interface RemoteDataSource {

    fun getBannerDataFromApi(): Observable<BannersResponse>

    fun getCatalogDataFromApi(): Observable<CatalogResponse>
}