package com.task.sarytask.domain.idlingresource

import android.util.Log
import androidx.test.espresso.idling.CountingIdlingResource

object EspressoIdlingResource {
    private const val RESOURCE = "GLOBAL"
    val idlingResource = CountingIdlingResource(RESOURCE)
    fun increment() {
        Log.e("Idle", "Increment")
        idlingResource.increment()
    }

    fun decrement() {
        Log.e("Idle", "decrement")
        if (!idlingResource.isIdleNow) {
            idlingResource.decrement()
        }
    }
}
