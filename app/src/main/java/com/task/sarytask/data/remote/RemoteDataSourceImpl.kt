package com.task.sarytask.data.remote

import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.models.CatalogResponse
import com.task.sarytask.domain.RemoteDataSource
import com.task.sarytask.domain.SchedulerProviders
import io.reactivex.Observable

/**
Created By M.EL-Wakeel
 */
class RemoteDataSourceImpl(
    private val api: ApiServices,
    private val scheduler: SchedulerProviders
) : RemoteDataSource {
    override fun getBannerDataFromApi(): Observable<BannersResponse> =
        api.fetchBanners()
            .subscribeOn(scheduler.io())

    override fun getCatalogDataFromApi(): Observable<CatalogResponse> =
        api.fetchCatalogs()
            .subscribeOn(scheduler.io())

}