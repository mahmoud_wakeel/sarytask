package com.task.sarytask.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
Created By M.EL-Wakeel
 */
@Parcelize
data class Result(
    @SerializedName("image")
    val image: String? = "",
    @SerializedName("link")
    val link: String? = ""
):Parcelable

data class BannersResponse(
    @SerializedName("result")
    val result: ArrayList<Result>? = ArrayList()
)