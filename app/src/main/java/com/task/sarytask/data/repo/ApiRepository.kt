package com.task.sarytask.data.repo

import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.models.CatalogResponse
import com.task.sarytask.data.remote.RemoteDataSourceImpl
import com.task.sarytask.domain.RemoteDataSource
import io.reactivex.Observable

/**
Created By M.EL-Wakeel
 */
class ApiRepository(
    private val remoteImpl: RemoteDataSourceImpl
) : RemoteDataSource {
    override fun getBannerDataFromApi(): Observable<BannersResponse> =
        Observable.concatArrayEager(remoteImpl.getBannerDataFromApi())

    override fun getCatalogDataFromApi(): Observable<CatalogResponse> =
        Observable.concatArrayEager(remoteImpl.getCatalogDataFromApi())
}