package com.task.sarytask.data.remote

import com.task.sarytask.data.models.BannersResponse
import com.task.sarytask.data.models.CatalogResponse
import io.reactivex.Observable
import retrofit2.http.GET

/**
Created By M.EL-Wakeel
 */
interface ApiServices {
    @GET("v2.5.1/baskets/76097/banners")
    fun fetchBanners(): Observable<BannersResponse>

    @GET ("baskets/76097/catalog")
    fun fetchCatalogs():Observable<CatalogResponse>
}