package com.task.sarytask.data.models

import com.google.gson.annotations.SerializedName

/**
Created By M.EL-Wakeel
 */
data class Data(
    @SerializedName("name")
    val name: String? = "",
    @SerializedName("image")
    val image: String? = ""
)

data class CatalogResult(
    @SerializedName("id")
    val id: Int? = 0,
    @SerializedName("data")
    val data: ArrayList<Data>? = ArrayList(),
    @SerializedName("data_type")
    val dataType: String? = "",
    @SerializedName("show_title")
    val showTitle: Boolean? = true,
    @SerializedName("title")
    val title: String? = "",
    @SerializedName("ui_type")
    val uiType: String? = "",
    @SerializedName("row_count")
    val rowCount: Int? = 0
)

data class CatalogResponse(
    @SerializedName("result")
    val result: ArrayList<CatalogResult>? = ArrayList()
)