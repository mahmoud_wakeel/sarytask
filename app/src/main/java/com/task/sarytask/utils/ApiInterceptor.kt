package com.task.sarytask.utils

import okhttp3.Interceptor
import okhttp3.Response

/**
Created By M.EL-Wakeel
 */
class ApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response = chain.run {
        proceed(
            request()
                .newBuilder()
                .addHeader("Device-Type", "android")
                .addHeader("App-Version", "3.1.1.0.0")
                .addHeader("Accept-Language", "en")
                .addHeader(
                    "Authorization",
                    "token eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ODg2NiwidXNlcl9waG9uZSI6Ijk2NjU2NDk4OTI1MCJ9.VYE28vtnMRLmwBISgvvnhOmPuGueW49ogOhXm5ZqsGU"
                )
                .build()
        )
    }
}