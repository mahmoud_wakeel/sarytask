package com.task.sarytask.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide

object Utils {
    fun loadImage(context: Context?, imageView: ImageView, url: String) {
        Glide.with(context!!)
            .load(url)
            .into(imageView)
    }
}