package com.task.sarytask.utils

/**
Created By M.EL-Wakeel
 */
object Constants {
    const val BASE_URL = "https://staging.sary.co/api/"
    const val BANNER = "banner"
    const val SMART = "smart"
    const val GROUP = "group"
}